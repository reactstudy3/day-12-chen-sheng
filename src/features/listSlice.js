import { createSlice } from "@reduxjs/toolkit"
const listSlice = createSlice(
   
    {
    name:'list',
    initialState:{
        todoList:[],
        inputValue:''
    },
    reducers:{
        updateInputValue:(state,action)=>{
            console.log(state,action);
            state.inputValue = action.payload

        },
        updateList:(state,action)=>{
            console.log(state,action);
            state.todoList.push(action.payload)
        },
        updateStatus:(state,action)=>{
            state.todoList.forEach(item=>{
                return item.id == action.payload ? item.done = !item.done : item.done
            }
            )
        },
        deleteItem:(state,action)=>{
            const index = state.todoList.indexOf(action.payload)
            state.todoList.splice(index,1)
        }

    },
  
})
export default listSlice.reducer
export const {updateList,updateInputValue,updateStatus,deleteItem} = listSlice.actions