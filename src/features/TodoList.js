import TodoGenerator from "./TodoGenerator"
import TodoGroup from "./TodoGroup"
import React from 'react'
import { useState } from 'react';
const TodoList = ()=>{
    const[itemList,setItemList]=useState([])
   
    return (
        <div className="form">
        <h2>TodoList</h2>
        <div className="todoGroup">
        <TodoGroup  itemList={itemList}/>
        </div>
        <TodoGenerator  itemList = {itemList} setItemList={setItemList}/>
        </div>
    )
}
export default TodoList