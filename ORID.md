## O:

Learned HTML CSS and React basic today

## R:

I feel fine.

## I:

- ***HTML***：Reviewed the use of semantic tags and the box structure, the box by divided into content,margin,padding,border. semantic tags are good for SEO
- ***CSS***：Learned three kinds of selectors, type, id and class. different selectors have different priorities, id>class>type. also learned the elastic box layout
- ***React basic***:Learned the basic syntax of JSX in React and component encapsulation, the difficulty of today's learning lies in the component value passing, props attribute is easy to confuse a lot of

## D:

Although I am the main front-end, I mainly use Vue2 and Vue3, and I still need to familiarize myself with React a little more

